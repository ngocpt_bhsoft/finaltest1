import {put, takeLatest} from 'redux-saga/effects';
import {actions, ActionTypes} from '../action';

function* getCacheCode() {
  try {
    console.log('test');
  } catch (e) {
    console.log({e});
  }
}

export function* watchGetCacheCode() {
  yield takeLatest(ActionTypes.TEST_REDUCER, getCacheCode);
}

function* getCacheCodeTest() {
  try {
    console.log('test');
  } catch (e) {
    console.log({e});
  }
}

export function* watchGetCacheCodeTest() {
  yield takeLatest(ActionTypes.TEST_REDUCER, getCacheCodeTest);
}
