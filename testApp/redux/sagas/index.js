import {all} from 'redux-saga/effects';
import {watchGetCacheCode, watchGetCacheCodeTest} from './app-saga';

function* rootSaga() {
  yield all([watchGetCacheCode(), watchGetCacheCodeTest()]);
}

export default rootSaga;
