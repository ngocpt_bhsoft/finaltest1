export const ActionTypes = {
  TEST_REDUCER: 'TEST_REDUCER',
};

export const actions = {
  testReducer: function() {
    return {
      type: ActionTypes.TEST_REDUCER,
    };
  },
};
